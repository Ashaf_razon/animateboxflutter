import 'package:flutter/material.dart';

class AnimatedBox extends StatefulWidget {
  const AnimatedBox({Key? key}) : super(key: key);

  @override
  _AnimatedBoxState createState() => _AnimatedBoxState();
}

class _AnimatedBoxState extends State<AnimatedBox> {
  AlignmentGeometry _alignment = Alignment.topLeft;
  void m(){
    setState((){

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Animated Box',
        ),
      ),
      body: Container(
        padding: const EdgeInsets.only(top: 50),
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            AnimatedAlign(
              alignment: _alignment,
              duration: const Duration(milliseconds: 1000),
              child: Container(
                height: 100,
                width: 100,
                color: Colors.blue,
              ),
                onEnd: () {
                  setState(() {
                    _alignment = _alignment == Alignment.topLeft
                        ? Alignment.bottomRight
                        : _alignment = Alignment.topLeft;
                  });
                }
            ),
            ButtonTheme(
              minWidth: 100,
              height: 50,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              ),
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    _alignment = _alignment == Alignment.topLeft
                        ? Alignment.bottomRight
                        : _alignment = Alignment.topLeft;
                  });
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.purple.withOpacity(0.6),
                ),
                child: const Text(
                  'Animate Me',
                  style: TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.3,
                      color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
